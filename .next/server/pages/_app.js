/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./app/AppProvider.tsx":
/*!*****************************!*\
  !*** ./app/AppProvider.tsx ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"AppProvider\": () => (/* binding */ AppProvider)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ \"styled-components\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _styles_theme__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles/theme */ \"./styles/theme.ts\");\n/* harmony import */ var _styles_global__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../styles/global */ \"./styles/global.ts\");\n/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-toastify */ \"react-toastify\");\n/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_toastify__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var react_toastify_dist_ReactToastify_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-toastify/dist/ReactToastify.css */ \"./node_modules/react-toastify/dist/ReactToastify.css\");\n/* harmony import */ var react_toastify_dist_ReactToastify_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_toastify_dist_ReactToastify_css__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var slick_carousel_slick_slick_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! slick-carousel/slick/slick.css */ \"./node_modules/slick-carousel/slick/slick.css\");\n/* harmony import */ var slick_carousel_slick_slick_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(slick_carousel_slick_slick_css__WEBPACK_IMPORTED_MODULE_6__);\n/* harmony import */ var slick_carousel_slick_slick_theme_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! slick-carousel/slick/slick-theme.css */ \"./node_modules/slick-carousel/slick/slick-theme.css\");\n/* harmony import */ var slick_carousel_slick_slick_theme_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(slick_carousel_slick_slick_theme_css__WEBPACK_IMPORTED_MODULE_7__);\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! next/router */ \"next/router\");\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_8__);\n/* harmony import */ var nprogress__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! nprogress */ \"nprogress\");\n/* harmony import */ var nprogress__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(nprogress__WEBPACK_IMPORTED_MODULE_9__);\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-redux */ \"react-redux\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_10__);\n/* harmony import */ var _shared_store_store__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../shared/store/store */ \"./shared/store/store.ts\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_12__);\n/* harmony import */ var shared_hooks_useMounted__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! shared/hooks/useMounted */ \"./shared/hooks/useMounted.ts\");\n/* harmony import */ var shared_hooks_useLangChange__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! shared/hooks/useLangChange */ \"./shared/hooks/useLangChange.ts\");\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nnext_router__WEBPACK_IMPORTED_MODULE_8___default().events.on(\"routeChangeStart\", (url)=>{\n    nprogress__WEBPACK_IMPORTED_MODULE_9___default().start();\n});\nnext_router__WEBPACK_IMPORTED_MODULE_8___default().events.on(\"routeChangeComplete\", ()=>nprogress__WEBPACK_IMPORTED_MODULE_9___default().done()\n);\nnext_router__WEBPACK_IMPORTED_MODULE_8___default().events.on(\"routeChangeError\", ()=>nprogress__WEBPACK_IMPORTED_MODULE_9___default().done()\n);\nconst AppProvider = ({ children  })=>{\n    const mounted = (0,shared_hooks_useMounted__WEBPACK_IMPORTED_MODULE_13__.useMounted)();\n    const { langChange  } = (0,shared_hooks_useLangChange__WEBPACK_IMPORTED_MODULE_14__.useLangChange)();\n    (0,react__WEBPACK_IMPORTED_MODULE_12__.useEffect)(()=>{\n        langChange(\"az\");\n    }, []);\n    return(/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_redux__WEBPACK_IMPORTED_MODULE_10__.Provider, {\n        store: _shared_store_store__WEBPACK_IMPORTED_MODULE_11__.store,\n        children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(styled_components__WEBPACK_IMPORTED_MODULE_1__.ThemeProvider, {\n            theme: _styles_theme__WEBPACK_IMPORTED_MODULE_2__.theme,\n            children: [\n                /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_styles_global__WEBPACK_IMPORTED_MODULE_3__[\"default\"], {}, void 0, false, {\n                    fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/app/AppProvider.tsx\",\n                    lineNumber: 33,\n                    columnNumber: 9\n                }, undefined),\n                mounted && children,\n                /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_toastify__WEBPACK_IMPORTED_MODULE_4__.ToastContainer, {}, void 0, false, {\n                    fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/app/AppProvider.tsx\",\n                    lineNumber: 35,\n                    columnNumber: 9\n                }, undefined)\n            ]\n        }, void 0, true, {\n            fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/app/AppProvider.tsx\",\n            lineNumber: 32,\n            columnNumber: 7\n        }, undefined)\n    }, void 0, false, {\n        fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/app/AppProvider.tsx\",\n        lineNumber: 31,\n        columnNumber: 5\n    }, undefined));\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9hcHAvQXBwUHJvdmlkZXIudHN4LmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFpRDtBQUNWO0FBQ0c7QUFDSztBQUNEO0FBQ1A7QUFDTTtBQUNiO0FBQ0M7QUFDSztBQUNPO0FBQ1o7QUFDbUI7QUFDTTtBQUUxREksNERBQWdCLENBQUMsQ0FBa0Isb0JBQUdTLEdBQUcsR0FBSyxDQUFDO0lBQzdDUixzREFBZTtBQUNqQixDQUFDO0FBQ0RELDREQUFnQixDQUFDLENBQXFCLDBCQUFRQyxxREFBYzs7QUFDNURELDREQUFnQixDQUFDLENBQWtCLHVCQUFRQyxxREFBYzs7QUFFbEQsS0FBSyxDQUFDVyxXQUFXLElBQUksQ0FBQyxDQUFDQyxRQUFRLEVBQUMsQ0FBQyxHQUFLLENBQUM7SUFDNUMsS0FBSyxDQUFDQyxPQUFPLEdBQUdULG9FQUFVO0lBQzFCLEtBQUssQ0FBQyxDQUFDLENBQUNVLFVBQVUsRUFBQyxDQUFDLEdBQUdULDBFQUFhO0lBRXBDRixpREFBUyxLQUFPLENBQUM7UUFDZlcsVUFBVSxDQUFDLENBQUk7SUFDakIsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUVMLE1BQU0sNkVBQ0hiLGtEQUFRO1FBQUNDLEtBQUssRUFBRUEsdURBQUs7OEZBQ25CUCw0REFBYTtZQUFDQyxLQUFLLEVBQUVBLGdEQUFLOzs0RkFDeEJDLHNEQUFXOzs7OztnQkFDWGdCLE9BQU8sSUFBSUQsUUFBUTs0RkFDbkJkLDBEQUFjOzs7Ozs7Ozs7Ozs7Ozs7O0FBSXZCLENBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hcHAvQXBwUHJvdmlkZXIudHN4Pzk5NjEiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVGhlbWVQcm92aWRlciB9IGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xyXG5pbXBvcnQgeyB0aGVtZSB9IGZyb20gXCIuLi9zdHlsZXMvdGhlbWVcIjtcclxuaW1wb3J0IEdsb2JhbFN0eWxlIGZyb20gXCIuLi9zdHlsZXMvZ2xvYmFsXCI7XHJcbmltcG9ydCB7IFRvYXN0Q29udGFpbmVyIH0gZnJvbSBcInJlYWN0LXRvYXN0aWZ5XCI7XHJcbmltcG9ydCBcInJlYWN0LXRvYXN0aWZ5L2Rpc3QvUmVhY3RUb2FzdGlmeS5jc3NcIjtcclxuaW1wb3J0IFwic2xpY2stY2Fyb3VzZWwvc2xpY2svc2xpY2suY3NzXCI7XHJcbmltcG9ydCBcInNsaWNrLWNhcm91c2VsL3NsaWNrL3NsaWNrLXRoZW1lLmNzc1wiO1xyXG5pbXBvcnQgUm91dGVyIGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG5pbXBvcnQgTlByb2dyZXNzIGZyb20gXCJucHJvZ3Jlc3NcIjtcclxuaW1wb3J0IHsgUHJvdmlkZXIgfSBmcm9tIFwicmVhY3QtcmVkdXhcIjtcclxuaW1wb3J0IHsgc3RvcmUgfSBmcm9tIFwiLi4vc2hhcmVkL3N0b3JlL3N0b3JlXCI7XHJcbmltcG9ydCB7IHVzZUVmZmVjdCB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyB1c2VNb3VudGVkIH0gZnJvbSBcInNoYXJlZC9ob29rcy91c2VNb3VudGVkXCI7XHJcbmltcG9ydCB7IHVzZUxhbmdDaGFuZ2UgfSBmcm9tIFwic2hhcmVkL2hvb2tzL3VzZUxhbmdDaGFuZ2VcIjtcclxuXHJcblJvdXRlci5ldmVudHMub24oXCJyb3V0ZUNoYW5nZVN0YXJ0XCIsICh1cmwpID0+IHtcclxuICBOUHJvZ3Jlc3Muc3RhcnQoKTtcclxufSk7XHJcblJvdXRlci5ldmVudHMub24oXCJyb3V0ZUNoYW5nZUNvbXBsZXRlXCIsICgpID0+IE5Qcm9ncmVzcy5kb25lKCkpO1xyXG5Sb3V0ZXIuZXZlbnRzLm9uKFwicm91dGVDaGFuZ2VFcnJvclwiLCAoKSA9PiBOUHJvZ3Jlc3MuZG9uZSgpKTtcclxuXHJcbmV4cG9ydCBjb25zdCBBcHBQcm92aWRlciA9ICh7IGNoaWxkcmVuIH0pID0+IHtcclxuICBjb25zdCBtb3VudGVkID0gdXNlTW91bnRlZCgpO1xyXG4gIGNvbnN0IHsgbGFuZ0NoYW5nZSB9ID0gdXNlTGFuZ0NoYW5nZSgpO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgbGFuZ0NoYW5nZShcImF6XCIpO1xyXG4gIH0sIFtdKTtcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxQcm92aWRlciBzdG9yZT17c3RvcmV9PlxyXG4gICAgICA8VGhlbWVQcm92aWRlciB0aGVtZT17dGhlbWV9PlxyXG4gICAgICAgIDxHbG9iYWxTdHlsZSAvPlxyXG4gICAgICAgIHttb3VudGVkICYmIGNoaWxkcmVufVxyXG4gICAgICAgIDxUb2FzdENvbnRhaW5lciAvPlxyXG4gICAgICA8L1RoZW1lUHJvdmlkZXI+XHJcbiAgICA8L1Byb3ZpZGVyPlxyXG4gICk7XHJcbn07XHJcbiJdLCJuYW1lcyI6WyJUaGVtZVByb3ZpZGVyIiwidGhlbWUiLCJHbG9iYWxTdHlsZSIsIlRvYXN0Q29udGFpbmVyIiwiUm91dGVyIiwiTlByb2dyZXNzIiwiUHJvdmlkZXIiLCJzdG9yZSIsInVzZUVmZmVjdCIsInVzZU1vdW50ZWQiLCJ1c2VMYW5nQ2hhbmdlIiwiZXZlbnRzIiwib24iLCJ1cmwiLCJzdGFydCIsImRvbmUiLCJBcHBQcm92aWRlciIsImNoaWxkcmVuIiwibW91bnRlZCIsImxhbmdDaGFuZ2UiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./app/AppProvider.tsx\n");

/***/ }),

/***/ "./next-i18next.config.js":
/*!********************************!*\
  !*** ./next-i18next.config.js ***!
  \********************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("\nconst path = __webpack_require__(/*! path */ \"path\");\nmodule.exports = {\n    i18n: {\n        locales: [\n            \"az\",\n            \"en\"\n        ],\n        defaultLocale: \"az\"\n    },\n    localePath: path.resolve(\"./public/locales\")\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9uZXh0LWkxOG5leHQuY29uZmlnLmpzLmpzIiwibWFwcGluZ3MiOiI7QUFBQSxLQUFLLENBQUNBLElBQUksR0FBR0MsbUJBQU8sQ0FBQyxrQkFBTTtBQUUzQkMsTUFBTSxDQUFDQyxPQUFPLEdBQUcsQ0FBQztJQUNoQkMsSUFBSSxFQUFFLENBQUM7UUFDTEMsT0FBTyxFQUFFLENBQUM7WUFBQSxDQUFJO1lBQUUsQ0FBSTtRQUFBLENBQUM7UUFDckJDLGFBQWEsRUFBRSxDQUFJO0lBQ3JCLENBQUM7SUFDREMsVUFBVSxFQUFFUCxJQUFJLENBQUNRLE9BQU8sQ0FBQyxDQUFrQjtBQUM3QyxDQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vbmV4dC1pMThuZXh0LmNvbmZpZy5qcz8xY2I5Il0sInNvdXJjZXNDb250ZW50IjpbImNvbnN0IHBhdGggPSByZXF1aXJlKFwicGF0aFwiKTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0ge1xyXG4gIGkxOG46IHtcclxuICAgIGxvY2FsZXM6IFtcImF6XCIsIFwiZW5cIl0sXHJcbiAgICBkZWZhdWx0TG9jYWxlOiBcImF6XCIsXHJcbiAgfSxcclxuICBsb2NhbGVQYXRoOiBwYXRoLnJlc29sdmUoXCIuL3B1YmxpYy9sb2NhbGVzXCIpLFxyXG59O1xyXG4iXSwibmFtZXMiOlsicGF0aCIsInJlcXVpcmUiLCJtb2R1bGUiLCJleHBvcnRzIiwiaTE4biIsImxvY2FsZXMiLCJkZWZhdWx0TG9jYWxlIiwibG9jYWxlUGF0aCIsInJlc29sdmUiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./next-i18next.config.js\n");

/***/ }),

/***/ "./pages/_app.tsx":
/*!************************!*\
  !*** ./pages/_app.tsx ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _app_AppProvider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app/AppProvider */ \"./app/AppProvider.tsx\");\n/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/head */ \"next/head\");\n/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _next_i18next_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../next-i18next.config */ \"./next-i18next.config.js\");\n/* harmony import */ var _next_i18next_config__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_next_i18next_config__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var styles_theme_scss__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! styles/theme.scss */ \"./styles/theme.scss\");\n/* harmony import */ var styles_theme_scss__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(styles_theme_scss__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var next_i18next__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next-i18next */ \"next-i18next\");\n/* harmony import */ var next_i18next__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_i18next__WEBPACK_IMPORTED_MODULE_6__);\n\n\n\n\n\n\n\nconst MyApp = ({ Component , pageProps  })=>{\n    return(/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, {\n        children: [\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_head__WEBPACK_IMPORTED_MODULE_3___default()), {\n                children: [\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"meta\", {\n                        httpEquiv: \"content-language\",\n                        content: \"az\"\n                    }, void 0, false, {\n                        fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                        lineNumber: 13,\n                        columnNumber: 9\n                    }, undefined),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"meta\", {\n                        name: \"publisher\",\n                        content: \"tech.az\"\n                    }, void 0, false, {\n                        fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                        lineNumber: 14,\n                        columnNumber: 9\n                    }, undefined),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"meta\", {\n                        name: \"viewport\",\n                        content: \"minimum-scale=1, initial-scale=1, width=device-width\"\n                    }, void 0, false, {\n                        fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                        lineNumber: 15,\n                        columnNumber: 9\n                    }, undefined),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"meta\", {\n                        name: \"description\",\n                        content: \"tech az. blog platformasi. technologiyalarla bagli xeberler. en son xeberler\"\n                    }, void 0, false, {\n                        fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                        lineNumber: 19,\n                        columnNumber: 9\n                    }, undefined),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"meta\", {\n                        name: \"description\",\n                        content: \"yerli startap və texnologiya ekosisteminə beynəlxalq təcr\\xfcbə və təcr\\xfcbə gətirən texnologiya mediası və tədbir platforması.\"\n                    }, void 0, false, {\n                        fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                        lineNumber: 23,\n                        columnNumber: 9\n                    }, undefined),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_head__WEBPACK_IMPORTED_MODULE_3___default()), {\n                        children: [\n                            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"meta\", {\n                                property: \"og:title\",\n                                content: \"tech az. blog platformasi. technologiyalarla bagli xeberler. en son xeberler\"\n                            }, void 0, false, {\n                                fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                                lineNumber: 28,\n                                columnNumber: 11\n                            }, undefined),\n                            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"meta\", {\n                                property: \"og:description\",\n                                content: \"yerli startap və texnologiya ekosisteminə beynəlxalq təcr\\xfcbə və təcr\\xfcbə gətirən texnologiya mediası və tədbir platforması.\"\n                            }, void 0, false, {\n                                fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                                lineNumber: 32,\n                                columnNumber: 11\n                            }, undefined),\n                            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"meta\", {\n                                property: \"og:type\",\n                                content: \"website\"\n                            }, void 0, false, {\n                                fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                                lineNumber: 36,\n                                columnNumber: 11\n                            }, undefined),\n                            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"meta\", {\n                                property: \"og:image\",\n                                content: \"/static/images/techaz.jpg\"\n                            }, void 0, false, {\n                                fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                                lineNumber: 37,\n                                columnNumber: 11\n                            }, undefined),\n                            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"meta\", {\n                                property: \"og:url\",\n                                content: \"https://www.tech.az\"\n                            }, void 0, false, {\n                                fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                                lineNumber: 38,\n                                columnNumber: 11\n                            }, undefined)\n                        ]\n                    }, void 0, true, {\n                        fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                        lineNumber: 27,\n                        columnNumber: 9\n                    }, undefined),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"meta\", {\n                        name: \"page-topic\",\n                        content: \"Media\"\n                    }, void 0, false, {\n                        fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                        lineNumber: 40,\n                        columnNumber: 9\n                    }, undefined),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"meta\", {\n                        name: \"page-type\",\n                        content: \"Blogging\"\n                    }, void 0, false, {\n                        fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                        lineNumber: 41,\n                        columnNumber: 9\n                    }, undefined),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"meta\", {\n                        name: \"audience\",\n                        content: \"Everyone\"\n                    }, void 0, false, {\n                        fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                        lineNumber: 42,\n                        columnNumber: 9\n                    }, undefined),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"title\", {\n                        children: \"tech.az\"\n                    }, void 0, false, {\n                        fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                        lineNumber: 43,\n                        columnNumber: 9\n                    }, undefined)\n                ]\n            }, void 0, true, {\n                fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                lineNumber: 12,\n                columnNumber: 7\n            }, undefined),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_app_AppProvider__WEBPACK_IMPORTED_MODULE_2__.AppProvider, {\n                children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, {\n                    ...pageProps\n                }, void 0, false, {\n                    fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                    lineNumber: 46,\n                    columnNumber: 9\n                }, undefined)\n            }, void 0, false, {\n                fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n                lineNumber: 45,\n                columnNumber: 7\n            }, undefined)\n        ]\n    }, void 0, true, {\n        fileName: \"/Users/rahimlisarkhan/Documents/github/supvc/tech.az/pages/_app.tsx\",\n        lineNumber: 11,\n        columnNumber: 5\n    }, undefined));\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,next_i18next__WEBPACK_IMPORTED_MODULE_6__.appWithTranslation)(MyApp, (_next_i18next_config__WEBPACK_IMPORTED_MODULE_4___default())));\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9fYXBwLnRzeC5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDZ0M7QUFDZ0I7QUFDcEI7QUFDMEI7QUFDNUI7QUFDdUI7QUFFakQsS0FBSyxDQUFDSyxLQUFLLElBQUksQ0FBQyxDQUFDQyxTQUFTLEdBQUVDLFNBQVMsRUFBVyxDQUFDLEdBQUssQ0FBQztJQUNyRCxNQUFNLDZFQUNIUCwyQ0FBUTs7d0ZBQ05FLGtEQUFJOztnR0FDRk0sQ0FBSTt3QkFBQ0MsU0FBUyxFQUFDLENBQWtCO3dCQUFDQyxPQUFPLEVBQUMsQ0FBSTs7Ozs7O2dHQUM5Q0YsQ0FBSTt3QkFBQ0csSUFBSSxFQUFDLENBQVc7d0JBQUNELE9BQU8sRUFBQyxDQUFTOzs7Ozs7Z0dBQ3ZDRixDQUFJO3dCQUNIRyxJQUFJLEVBQUMsQ0FBVTt3QkFDZkQsT0FBTyxFQUFDLENBQXNEOzs7Ozs7Z0dBRS9ERixDQUFJO3dCQUNIRyxJQUFJLEVBQUMsQ0FBYTt3QkFDbEJELE9BQU8sRUFBQyxDQUE4RTs7Ozs7O2dHQUV2RkYsQ0FBSTt3QkFDSEcsSUFBSSxFQUFDLENBQWE7d0JBQ2xCRCxPQUFPLEVBQUMsQ0FBNEg7Ozs7OztnR0FFckhSLGtEQUFaOzt3R0FDRk0sQ0FBSTtnQ0FDSEksUUFBUSxFQUFDLENBQVU7Z0NBQ25CRixPQUFPLEVBQUMsQ0FBOEU7Ozs7Ozt3R0FFdkZGLENBQUk7Z0NBQ0hJLFFBQVEsRUFBQyxDQUFnQjtnQ0FDekJGLE9BQU8sRUFBQyxDQUE0SDs7Ozs7O3dHQUVySUYsQ0FBSTtnQ0FBQ0ksUUFBUSxFQUFDLENBQVM7Z0NBQUNGLE9BQU8sRUFBQyxDQUFTOzs7Ozs7d0dBQ3pDRixDQUFJO2dDQUFDSSxRQUFRLEVBQUMsQ0FBVTtnQ0FBQ0YsT0FBTyxFQUFDLENBQTJCOzs7Ozs7d0dBQzVERixDQUFJO2dDQUFDSSxRQUFRLEVBQUMsQ0FBUTtnQ0FBQ0YsT0FBTyxFQUFDLENBQXFCOzs7Ozs7Ozs7Ozs7Z0dBRXRERixDQUFJO3dCQUFDRyxJQUFJLEVBQUMsQ0FBWTt3QkFBQ0QsT0FBTyxFQUFDLENBQU87Ozs7OztnR0FDdENGLENBQUk7d0JBQUNHLElBQUksRUFBQyxDQUFXO3dCQUFDRCxPQUFPLEVBQUMsQ0FBVTs7Ozs7O2dHQUN4Q0YsQ0FBSTt3QkFBQ0csSUFBSSxFQUFDLENBQVU7d0JBQUNELE9BQU8sRUFBQyxDQUFVOzs7Ozs7Z0dBQ3ZDRyxDQUFLO2tDQUFDLENBQU87Ozs7Ozs7Ozs7Ozt3RkFFZloseURBQVc7c0dBQ1RLLFNBQVM7dUJBQUtDLFNBQVM7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBSWhDLENBQUM7QUFFRCxpRUFBZUgsZ0VBQWtCLENBQUNDLEtBQUssRUFBRUYsNkRBQWlCLENBQUMsRUFBQyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3BhZ2VzL19hcHAudHN4PzJmYmUiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHR5cGUgeyBBcHBQcm9wcyAvKiwgQXBwQ29udGV4dCAqLyB9IGZyb20gXCJuZXh0L2FwcFwiO1xyXG5pbXBvcnQgeyBGcmFnbWVudCB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBBcHBQcm92aWRlciB9IGZyb20gXCIuLi9hcHAvQXBwUHJvdmlkZXJcIjtcclxuaW1wb3J0IEhlYWQgZnJvbSBcIm5leHQvaGVhZFwiO1xyXG5pbXBvcnQgbmV4dEkxOE5leHRDb25maWcgZnJvbSBcIi4uL25leHQtaTE4bmV4dC5jb25maWdcIjtcclxuaW1wb3J0IFwic3R5bGVzL3RoZW1lLnNjc3NcIjtcclxuaW1wb3J0IHsgYXBwV2l0aFRyYW5zbGF0aW9uIH0gZnJvbSBcIm5leHQtaTE4bmV4dFwiO1xyXG5cclxuY29uc3QgTXlBcHAgPSAoeyBDb21wb25lbnQsIHBhZ2VQcm9wcyB9OiBBcHBQcm9wcykgPT4ge1xyXG4gIHJldHVybiAoXHJcbiAgICA8RnJhZ21lbnQ+XHJcbiAgICAgIDxIZWFkPlxyXG4gICAgICAgIDxtZXRhIGh0dHBFcXVpdj1cImNvbnRlbnQtbGFuZ3VhZ2VcIiBjb250ZW50PVwiYXpcIiAvPlxyXG4gICAgICAgIDxtZXRhIG5hbWU9XCJwdWJsaXNoZXJcIiBjb250ZW50PVwidGVjaC5helwiIC8+XHJcbiAgICAgICAgPG1ldGFcclxuICAgICAgICAgIG5hbWU9XCJ2aWV3cG9ydFwiXHJcbiAgICAgICAgICBjb250ZW50PVwibWluaW11bS1zY2FsZT0xLCBpbml0aWFsLXNjYWxlPTEsIHdpZHRoPWRldmljZS13aWR0aFwiXHJcbiAgICAgICAgLz5cclxuICAgICAgICA8bWV0YVxyXG4gICAgICAgICAgbmFtZT1cImRlc2NyaXB0aW9uXCJcclxuICAgICAgICAgIGNvbnRlbnQ9XCJ0ZWNoIGF6LiBibG9nIHBsYXRmb3JtYXNpLiB0ZWNobm9sb2dpeWFsYXJsYSBiYWdsaSB4ZWJlcmxlci4gZW4gc29uIHhlYmVybGVyXCJcclxuICAgICAgICAvPlxyXG4gICAgICAgIDxtZXRhXHJcbiAgICAgICAgICBuYW1lPVwiZGVzY3JpcHRpb25cIlxyXG4gICAgICAgICAgY29udGVudD1cInllcmxpIHN0YXJ0YXAgdsmZIHRleG5vbG9naXlhIGVrb3Npc3RlbWluyZkgYmV5bsmZbHhhbHEgdMmZY3LDvGLJmSB2yZkgdMmZY3LDvGLJmSBnyZl0aXLJmW4gdGV4bm9sb2dpeWEgbWVkaWFzxLEgdsmZIHTJmWRiaXIgcGxhdGZvcm1hc8SxLlwiXHJcbiAgICAgICAgLz5cclxuICAgICAgICA8SGVhZD5cclxuICAgICAgICAgIDxtZXRhXHJcbiAgICAgICAgICAgIHByb3BlcnR5PVwib2c6dGl0bGVcIlxyXG4gICAgICAgICAgICBjb250ZW50PVwidGVjaCBhei4gYmxvZyBwbGF0Zm9ybWFzaS4gdGVjaG5vbG9naXlhbGFybGEgYmFnbGkgeGViZXJsZXIuIGVuIHNvbiB4ZWJlcmxlclwiXHJcbiAgICAgICAgICAvPlxyXG4gICAgICAgICAgPG1ldGFcclxuICAgICAgICAgICAgcHJvcGVydHk9XCJvZzpkZXNjcmlwdGlvblwiXHJcbiAgICAgICAgICAgIGNvbnRlbnQ9XCJ5ZXJsaSBzdGFydGFwIHbJmSB0ZXhub2xvZ2l5YSBla29zaXN0ZW1pbsmZIGJleW7JmWx4YWxxIHTJmWNyw7xiyZkgdsmZIHTJmWNyw7xiyZkgZ8mZdGlyyZluIHRleG5vbG9naXlhIG1lZGlhc8SxIHbJmSB0yZlkYmlyIHBsYXRmb3JtYXPEsS5cIlxyXG4gICAgICAgICAgLz5cclxuICAgICAgICAgIDxtZXRhIHByb3BlcnR5PVwib2c6dHlwZVwiIGNvbnRlbnQ9XCJ3ZWJzaXRlXCIgLz5cclxuICAgICAgICAgIDxtZXRhIHByb3BlcnR5PVwib2c6aW1hZ2VcIiBjb250ZW50PVwiL3N0YXRpYy9pbWFnZXMvdGVjaGF6LmpwZ1wiIC8+XHJcbiAgICAgICAgICA8bWV0YSBwcm9wZXJ0eT1cIm9nOnVybFwiIGNvbnRlbnQ9XCJodHRwczovL3d3dy50ZWNoLmF6XCIgLz5cclxuICAgICAgICA8L0hlYWQ+XHJcbiAgICAgICAgPG1ldGEgbmFtZT1cInBhZ2UtdG9waWNcIiBjb250ZW50PVwiTWVkaWFcIiAvPlxyXG4gICAgICAgIDxtZXRhIG5hbWU9XCJwYWdlLXR5cGVcIiBjb250ZW50PVwiQmxvZ2dpbmdcIiAvPlxyXG4gICAgICAgIDxtZXRhIG5hbWU9XCJhdWRpZW5jZVwiIGNvbnRlbnQ9XCJFdmVyeW9uZVwiIC8+XHJcbiAgICAgICAgPHRpdGxlPnRlY2guYXo8L3RpdGxlPlxyXG4gICAgICA8L0hlYWQ+XHJcbiAgICAgIDxBcHBQcm92aWRlcj5cclxuICAgICAgICA8Q29tcG9uZW50IHsuLi5wYWdlUHJvcHN9IC8+XHJcbiAgICAgIDwvQXBwUHJvdmlkZXI+XHJcbiAgICA8L0ZyYWdtZW50PlxyXG4gICk7XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBhcHBXaXRoVHJhbnNsYXRpb24oTXlBcHAsIG5leHRJMThOZXh0Q29uZmlnKTtcclxuIl0sIm5hbWVzIjpbIkZyYWdtZW50IiwiQXBwUHJvdmlkZXIiLCJIZWFkIiwibmV4dEkxOE5leHRDb25maWciLCJhcHBXaXRoVHJhbnNsYXRpb24iLCJNeUFwcCIsIkNvbXBvbmVudCIsInBhZ2VQcm9wcyIsIm1ldGEiLCJodHRwRXF1aXYiLCJjb250ZW50IiwibmFtZSIsInByb3BlcnR5IiwidGl0bGUiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/_app.tsx\n");

/***/ }),

/***/ "./shared/hooks/useLangChange.ts":
/*!***************************************!*\
  !*** ./shared/hooks/useLangChange.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"useLangChange\": () => (/* binding */ useLangChange)\n/* harmony export */ });\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! next/router */ \"next/router\");\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_0__);\n\nconst useLangChange = ()=>{\n    const { asPath , locale , replace  } = (0,next_router__WEBPACK_IMPORTED_MODULE_0__.useRouter)();\n    const langChange = (lang)=>{\n        replace(asPath, asPath, {\n            locale: lang\n        });\n    };\n    return {\n        lang: locale,\n        langChange\n    };\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zaGFyZWQvaG9va3MvdXNlTGFuZ0NoYW5nZS50cy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7QUFBdUM7QUFFaEMsS0FBSyxDQUFDQyxhQUFhLE9BQVMsQ0FBQztJQUNsQyxLQUFLLENBQUMsQ0FBQyxDQUFDQyxNQUFNLEdBQUVDLE1BQU0sR0FBRUMsT0FBTyxFQUFDLENBQUMsR0FBR0osc0RBQVM7SUFFN0MsS0FBSyxDQUFDSyxVQUFVLElBQUlDLElBQVksR0FBSyxDQUFDO1FBQ3BDRixPQUFPLENBQUNGLE1BQU0sRUFBRUEsTUFBTSxFQUFFLENBQUM7WUFBQ0MsTUFBTSxFQUFFRyxJQUFJO1FBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRUQsTUFBTSxDQUFDLENBQUM7UUFBQ0EsSUFBSSxFQUFFSCxNQUFNO1FBQUVFLFVBQVU7SUFBQyxDQUFDO0FBQ3JDLENBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zaGFyZWQvaG9va3MvdXNlTGFuZ0NoYW5nZS50cz8yODIxIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG5cclxuZXhwb3J0IGNvbnN0IHVzZUxhbmdDaGFuZ2UgPSAoKSA9PiB7XHJcbiAgY29uc3QgeyBhc1BhdGgsIGxvY2FsZSwgcmVwbGFjZSB9ID0gdXNlUm91dGVyKCk7XHJcblxyXG4gIGNvbnN0IGxhbmdDaGFuZ2UgPSAobGFuZzogc3RyaW5nKSA9PiB7XHJcbiAgICByZXBsYWNlKGFzUGF0aCwgYXNQYXRoLCB7IGxvY2FsZTogbGFuZyB9KTtcclxuICB9O1xyXG5cclxuICByZXR1cm4geyBsYW5nOiBsb2NhbGUsIGxhbmdDaGFuZ2UgfTtcclxufTtcclxuIl0sIm5hbWVzIjpbInVzZVJvdXRlciIsInVzZUxhbmdDaGFuZ2UiLCJhc1BhdGgiLCJsb2NhbGUiLCJyZXBsYWNlIiwibGFuZ0NoYW5nZSIsImxhbmciXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./shared/hooks/useLangChange.ts\n");

/***/ }),

/***/ "./shared/hooks/useMounted.ts":
/*!************************************!*\
  !*** ./shared/hooks/useMounted.ts ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"useMounted\": () => (/* binding */ useMounted)\n/* harmony export */ });\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_use__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-use */ \"react-use\");\n/* harmony import */ var react_use__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_use__WEBPACK_IMPORTED_MODULE_1__);\n\n\nconst useMounted = ()=>{\n    const mounted = (0,react__WEBPACK_IMPORTED_MODULE_0__.useRef)(false);\n    const update = (0,react_use__WEBPACK_IMPORTED_MODULE_1__.useUpdate)();\n    (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(()=>{\n        if (!mounted.current) {\n            mounted.current = true;\n            update();\n        }\n    }, [\n        update\n    ]);\n    return mounted.current;\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zaGFyZWQvaG9va3MvdXNlTW91bnRlZC50cy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUF5QztBQUNKO0FBRTlCLEtBQUssQ0FBQ0csVUFBVSxPQUFTLENBQUM7SUFDL0IsS0FBSyxDQUFDQyxPQUFPLEdBQUdILDZDQUFNLENBQUMsS0FBSztJQUM1QixLQUFLLENBQUNJLE1BQU0sR0FBR0gsb0RBQVM7SUFFeEJGLGdEQUFTLEtBQU8sQ0FBQztRQUNmLEVBQUUsR0FBR0ksT0FBTyxDQUFDRSxPQUFPLEVBQUUsQ0FBQztZQUNyQkYsT0FBTyxDQUFDRSxPQUFPLEdBQUcsSUFBSTtZQUN0QkQsTUFBTTtRQUNSLENBQUM7SUFDSCxDQUFDLEVBQUUsQ0FBQ0E7UUFBQUEsTUFBTTtJQUFBLENBQUM7SUFDWCxNQUFNLENBQUNELE9BQU8sQ0FBQ0UsT0FBTztBQUN4QixDQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vc2hhcmVkL2hvb2tzL3VzZU1vdW50ZWQudHM/MWFlOSJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VFZmZlY3QsIHVzZVJlZiB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyB1c2VVcGRhdGUgfSBmcm9tIFwicmVhY3QtdXNlXCI7XHJcblxyXG5leHBvcnQgY29uc3QgdXNlTW91bnRlZCA9ICgpID0+IHtcclxuICBjb25zdCBtb3VudGVkID0gdXNlUmVmKGZhbHNlKTtcclxuICBjb25zdCB1cGRhdGUgPSB1c2VVcGRhdGUoKTtcclxuICBcclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgaWYgKCFtb3VudGVkLmN1cnJlbnQpIHtcclxuICAgICAgbW91bnRlZC5jdXJyZW50ID0gdHJ1ZTtcclxuICAgICAgdXBkYXRlKCk7XHJcbiAgICB9XHJcbiAgfSwgW3VwZGF0ZV0pO1xyXG4gIHJldHVybiBtb3VudGVkLmN1cnJlbnQ7XHJcbn07XHJcbiJdLCJuYW1lcyI6WyJ1c2VFZmZlY3QiLCJ1c2VSZWYiLCJ1c2VVcGRhdGUiLCJ1c2VNb3VudGVkIiwibW91bnRlZCIsInVwZGF0ZSIsImN1cnJlbnQiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./shared/hooks/useMounted.ts\n");

/***/ }),

/***/ "./shared/store/slices/home/homeSlices.ts":
/*!************************************************!*\
  !*** ./shared/store/slices/home/homeSlices.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"homeSlice\": () => (/* binding */ homeSlice),\n/* harmony export */   \"setAppMode\": () => (/* binding */ setAppMode),\n/* harmony export */   \"fillAllData\": () => (/* binding */ fillAllData),\n/* harmony export */   \"fillNewsSlug\": () => (/* binding */ fillNewsSlug),\n/* harmony export */   \"fillAppMode\": () => (/* binding */ fillAppMode),\n/* harmony export */   \"setIsOpenSearch\": () => (/* binding */ setIsOpenSearch),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @reduxjs/toolkit */ \"@reduxjs/toolkit\");\n/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);\n\nconst initialState = {\n    appMode: false,\n    openSearchBar: false,\n    allData: [],\n    newsSlug: null\n};\nconst homeSlice = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createSlice)({\n    name: \"home\",\n    initialState,\n    reducers: {\n        setAppMode: (state)=>{\n            state.appMode = !state.appMode;\n            localStorage.setItem(\"appMode\", `${state.appMode}`);\n        },\n        setIsOpenSearch: (state)=>{\n            state.openSearchBar = !state.openSearchBar;\n        },\n        fillAppMode: (state)=>{\n            let mode = localStorage.getItem(\"appMode\");\n            state.appMode = mode === \"true\" ? true : false;\n        },\n        fillAllData: (state, action)=>{\n            state.allData = action.payload;\n        },\n        fillNewsSlug: (state, action)=>{\n            state.newsSlug = action.payload;\n        }\n    }\n});\n// Action creators are generated for each case reducer function\nconst { setAppMode , fillAllData , fillNewsSlug , fillAppMode , setIsOpenSearch ,  } = homeSlice.actions;\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (homeSlice.reducer);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zaGFyZWQvc3RvcmUvc2xpY2VzL2hvbWUvaG9tZVNsaWNlcy50cy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBNkQ7QUFFN0QsS0FBSyxDQUFDQyxZQUFZLEdBQUcsQ0FBQztJQUNwQkMsT0FBTyxFQUFFLEtBQUs7SUFDZEMsYUFBYSxFQUFFLEtBQUs7SUFDcEJDLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFDWEMsUUFBUSxFQUFFLElBQUk7QUFDaEIsQ0FBQztBQUVNLEtBQUssQ0FBQ0MsU0FBUyxHQUFHTiw2REFBVyxDQUFDLENBQUM7SUFDcENPLElBQUksRUFBRSxDQUFNO0lBQ1pOLFlBQVk7SUFDWk8sUUFBUSxFQUFFLENBQUM7UUFDVEMsVUFBVSxHQUFHQyxLQUFLLEdBQUssQ0FBQztZQUN0QkEsS0FBSyxDQUFDUixPQUFPLElBQUlRLEtBQUssQ0FBQ1IsT0FBTztZQUM5QlMsWUFBWSxDQUFDQyxPQUFPLENBQUMsQ0FBUyxhQUFLRixLQUFLLENBQUNSLE9BQU87UUFDbEQsQ0FBQztRQUNEVyxlQUFlLEdBQUdILEtBQUssR0FBSyxDQUFDO1lBQzNCQSxLQUFLLENBQUNQLGFBQWEsSUFBSU8sS0FBSyxDQUFDUCxhQUFhO1FBQzVDLENBQUM7UUFDRFcsV0FBVyxHQUFHSixLQUFLLEdBQUssQ0FBQztZQUN2QixHQUFHLENBQUNLLElBQUksR0FBR0osWUFBWSxDQUFDSyxPQUFPLENBQUMsQ0FBUztZQUV6Q04sS0FBSyxDQUFDUixPQUFPLEdBQUdhLElBQUksS0FBSyxDQUFNLFFBQUcsSUFBSSxHQUFHLEtBQUs7UUFDaEQsQ0FBQztRQUNERSxXQUFXLEdBQUdQLEtBQUssRUFBRVEsTUFBMEIsR0FBSyxDQUFDO1lBQ25EUixLQUFLLENBQUNOLE9BQU8sR0FBR2MsTUFBTSxDQUFDQyxPQUFPO1FBQ2hDLENBQUM7UUFDREMsWUFBWSxHQUFHVixLQUFLLEVBQUVRLE1BQTBCLEdBQUssQ0FBQztZQUNwRFIsS0FBSyxDQUFDTCxRQUFRLEdBQUdhLE1BQU0sQ0FBQ0MsT0FBTztRQUNqQyxDQUFDO0lBQ0gsQ0FBQztBQUNILENBQUM7QUFFRCxFQUErRDtBQUN4RCxLQUFLLENBQUMsQ0FBQyxDQUNaVixVQUFVLEdBQ1ZRLFdBQVcsR0FDWEcsWUFBWSxHQUNaTixXQUFXLEdBQ1hELGVBQWUsSUFDakIsQ0FBQyxHQUFHUCxTQUFTLENBQUNlLE9BQU87QUFFckIsaUVBQWVmLFNBQVMsQ0FBQ2dCLE9BQU8sRUFBQyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3NoYXJlZC9zdG9yZS9zbGljZXMvaG9tZS9ob21lU2xpY2VzLnRzPzBhYWQiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgY3JlYXRlU2xpY2UsIFBheWxvYWRBY3Rpb24gfSBmcm9tIFwiQHJlZHV4anMvdG9vbGtpdFwiO1xyXG5cclxuY29uc3QgaW5pdGlhbFN0YXRlID0ge1xyXG4gIGFwcE1vZGU6IGZhbHNlLFxyXG4gIG9wZW5TZWFyY2hCYXI6IGZhbHNlLFxyXG4gIGFsbERhdGE6IFtdLFxyXG4gIG5ld3NTbHVnOiBudWxsLFxyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGhvbWVTbGljZSA9IGNyZWF0ZVNsaWNlKHtcclxuICBuYW1lOiBcImhvbWVcIixcclxuICBpbml0aWFsU3RhdGUsXHJcbiAgcmVkdWNlcnM6IHtcclxuICAgIHNldEFwcE1vZGU6IChzdGF0ZSkgPT4ge1xyXG4gICAgICBzdGF0ZS5hcHBNb2RlID0gIXN0YXRlLmFwcE1vZGU7XHJcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiYXBwTW9kZVwiLCBgJHtzdGF0ZS5hcHBNb2RlfWApO1xyXG4gICAgfSxcclxuICAgIHNldElzT3BlblNlYXJjaDogKHN0YXRlKSA9PiB7XHJcbiAgICAgIHN0YXRlLm9wZW5TZWFyY2hCYXIgPSAhc3RhdGUub3BlblNlYXJjaEJhcjtcclxuICAgIH0sXHJcbiAgICBmaWxsQXBwTW9kZTogKHN0YXRlKSA9PiB7XHJcbiAgICAgIGxldCBtb2RlID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJhcHBNb2RlXCIpO1xyXG5cclxuICAgICAgc3RhdGUuYXBwTW9kZSA9IG1vZGUgPT09IFwidHJ1ZVwiID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgfSxcclxuICAgIGZpbGxBbGxEYXRhOiAoc3RhdGUsIGFjdGlvbjogUGF5bG9hZEFjdGlvbjxhbnk+KSA9PiB7XHJcbiAgICAgIHN0YXRlLmFsbERhdGEgPSBhY3Rpb24ucGF5bG9hZDtcclxuICAgIH0sXHJcbiAgICBmaWxsTmV3c1NsdWc6IChzdGF0ZSwgYWN0aW9uOiBQYXlsb2FkQWN0aW9uPGFueT4pID0+IHtcclxuICAgICAgc3RhdGUubmV3c1NsdWcgPSBhY3Rpb24ucGF5bG9hZDtcclxuICAgIH0sXHJcbiAgfSxcclxufSk7XHJcblxyXG4vLyBBY3Rpb24gY3JlYXRvcnMgYXJlIGdlbmVyYXRlZCBmb3IgZWFjaCBjYXNlIHJlZHVjZXIgZnVuY3Rpb25cclxuZXhwb3J0IGNvbnN0IHtcclxuICBzZXRBcHBNb2RlLFxyXG4gIGZpbGxBbGxEYXRhLFxyXG4gIGZpbGxOZXdzU2x1ZyxcclxuICBmaWxsQXBwTW9kZSxcclxuICBzZXRJc09wZW5TZWFyY2gsXHJcbn0gPSBob21lU2xpY2UuYWN0aW9ucztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGhvbWVTbGljZS5yZWR1Y2VyO1xyXG4iXSwibmFtZXMiOlsiY3JlYXRlU2xpY2UiLCJpbml0aWFsU3RhdGUiLCJhcHBNb2RlIiwib3BlblNlYXJjaEJhciIsImFsbERhdGEiLCJuZXdzU2x1ZyIsImhvbWVTbGljZSIsIm5hbWUiLCJyZWR1Y2VycyIsInNldEFwcE1vZGUiLCJzdGF0ZSIsImxvY2FsU3RvcmFnZSIsInNldEl0ZW0iLCJzZXRJc09wZW5TZWFyY2giLCJmaWxsQXBwTW9kZSIsIm1vZGUiLCJnZXRJdGVtIiwiZmlsbEFsbERhdGEiLCJhY3Rpb24iLCJwYXlsb2FkIiwiZmlsbE5ld3NTbHVnIiwiYWN0aW9ucyIsInJlZHVjZXIiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./shared/store/slices/home/homeSlices.ts\n");

/***/ }),

/***/ "./shared/store/slices/index.ts":
/*!**************************************!*\
  !*** ./shared/store/slices/index.ts ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"reducers\": () => (/* binding */ reducers)\n/* harmony export */ });\n/* harmony import */ var _home_homeSlices__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home/homeSlices */ \"./shared/store/slices/home/homeSlices.ts\");\n\nconst reducers = {\n    home: _home_homeSlices__WEBPACK_IMPORTED_MODULE_0__[\"default\"]\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zaGFyZWQvc3RvcmUvc2xpY2VzL2luZGV4LnRzLmpzIiwibWFwcGluZ3MiOiI7Ozs7O0FBQXlDO0FBR2xDLEtBQUssQ0FBQ0MsUUFBUSxHQUFHLENBQUM7SUFDckJDLElBQUksRUFBQ0Ysd0RBQVM7QUFDbEIsQ0FBQyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3NoYXJlZC9zdG9yZS9zbGljZXMvaW5kZXgudHM/Y2MxNyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgaG9tZVNsaWNlIGZyb20gXCIuL2hvbWUvaG9tZVNsaWNlc1wiXHJcblxyXG5cclxuZXhwb3J0IGNvbnN0IHJlZHVjZXJzID0ge1xyXG4gICAgaG9tZTpob21lU2xpY2VcclxufSAiXSwibmFtZXMiOlsiaG9tZVNsaWNlIiwicmVkdWNlcnMiLCJob21lIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./shared/store/slices/index.ts\n");

/***/ }),

/***/ "./shared/store/store.ts":
/*!*******************************!*\
  !*** ./shared/store/store.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"store\": () => (/* binding */ store)\n/* harmony export */ });\n/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @reduxjs/toolkit */ \"@reduxjs/toolkit\");\n/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _slices__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./slices */ \"./shared/store/slices/index.ts\");\n\n\nconst store = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.configureStore)({\n    reducer: _slices__WEBPACK_IMPORTED_MODULE_1__.reducers\n});\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zaGFyZWQvc3RvcmUvc3RvcmUudHMuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFpRDtBQUNkO0FBRTVCLEtBQUssQ0FBQ0UsS0FBSyxHQUFHRixnRUFBYyxDQUFDLENBQUM7SUFDbkNHLE9BQU8sRUFBRUYsNkNBQVE7QUFDbkIsQ0FBQyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3NoYXJlZC9zdG9yZS9zdG9yZS50cz81NDQwIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGNvbmZpZ3VyZVN0b3JlIH0gZnJvbSAnQHJlZHV4anMvdG9vbGtpdCdcclxuaW1wb3J0IHsgcmVkdWNlcnMgfSBmcm9tIFwiLi9zbGljZXNcIlxyXG5cclxuZXhwb3J0IGNvbnN0IHN0b3JlID0gY29uZmlndXJlU3RvcmUoe1xyXG4gIHJlZHVjZXI6IHJlZHVjZXJzLFxyXG59KVxyXG5cclxuXHJcbi8vIEluZmVyIHRoZSBgUm9vdFN0YXRlYCBhbmQgYEFwcERpc3BhdGNoYCB0eXBlcyBmcm9tIHRoZSBzdG9yZSBpdHNlbGZcclxuZXhwb3J0IHR5cGUgUm9vdFN0YXRlID0gUmV0dXJuVHlwZTx0eXBlb2Ygc3RvcmUuZ2V0U3RhdGU+XHJcbi8vIEluZmVycmVkIHR5cGU6IHtwb3N0czogUG9zdHNTdGF0ZSwgY29tbWVudHM6IENvbW1lbnRzU3RhdGUsIHVzZXJzOiBVc2Vyc1N0YXRlfVxyXG5leHBvcnQgdHlwZSBBcHBEaXNwYXRjaCA9IHR5cGVvZiBzdG9yZS5kaXNwYXRjaCJdLCJuYW1lcyI6WyJjb25maWd1cmVTdG9yZSIsInJlZHVjZXJzIiwic3RvcmUiLCJyZWR1Y2VyIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./shared/store/store.ts\n");

/***/ }),

/***/ "./styles/boxModel.ts":
/*!****************************!*\
  !*** ./styles/boxModel.ts ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"boxModel\": () => (/* binding */ boxModel)\n/* harmony export */ });\nconst boxModel = {\n    padding: {\n        small: '10px',\n        normal: '20px',\n        large: '30px'\n    },\n    margin: {\n        small: '10px',\n        normal: '20px',\n        large: '30px'\n    }\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zdHlsZXMvYm94TW9kZWwudHMuanMiLCJtYXBwaW5ncyI6Ijs7OztBQUFPLEtBQUssQ0FBQ0EsUUFBUSxHQUFHLENBQUM7SUFDckJDLE9BQU8sRUFBQyxDQUFDO1FBQ0xDLEtBQUssRUFBQyxDQUFNO1FBQ1pDLE1BQU0sRUFBQyxDQUFNO1FBQ2JDLEtBQUssRUFBQyxDQUFNO0lBQ2hCLENBQUM7SUFDREMsTUFBTSxFQUFDLENBQUM7UUFDSkgsS0FBSyxFQUFDLENBQU07UUFDWkMsTUFBTSxFQUFDLENBQU07UUFDYkMsS0FBSyxFQUFDLENBQU07SUFDaEIsQ0FBQztBQUNILENBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zdHlsZXMvYm94TW9kZWwudHM/ZWE0YSJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY29uc3QgYm94TW9kZWwgPSB7XHJcbiAgICBwYWRkaW5nOntcclxuICAgICAgICBzbWFsbDonMTBweCcsXHJcbiAgICAgICAgbm9ybWFsOicyMHB4JyxcclxuICAgICAgICBsYXJnZTonMzBweCcsXHJcbiAgICB9LFxyXG4gICAgbWFyZ2luOntcclxuICAgICAgICBzbWFsbDonMTBweCcsXHJcbiAgICAgICAgbm9ybWFsOicyMHB4JyxcclxuICAgICAgICBsYXJnZTonMzBweCcsXHJcbiAgICB9LFxyXG4gIH0iXSwibmFtZXMiOlsiYm94TW9kZWwiLCJwYWRkaW5nIiwic21hbGwiLCJub3JtYWwiLCJsYXJnZSIsIm1hcmdpbiJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./styles/boxModel.ts\n");

/***/ }),

/***/ "./styles/breakpoint.ts":
/*!******************************!*\
  !*** ./styles/breakpoint.ts ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"breakpoint\": () => (/* binding */ breakpoint)\n/* harmony export */ });\nconst breakpoint = {\n    mobile: \"375px\",\n    tablet: \"600px\",\n    laptop: \"1200px\",\n    desktop: \"1600px\"\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zdHlsZXMvYnJlYWtwb2ludC50cy5qcyIsIm1hcHBpbmdzIjoiOzs7O0FBQU8sS0FBSyxDQUFDQSxVQUFVLEdBQUcsQ0FBQztJQUN6QkMsTUFBTSxFQUFFLENBQU87SUFDZkMsTUFBTSxFQUFFLENBQU87SUFDZkMsTUFBTSxFQUFFLENBQVE7SUFDaEJDLE9BQU8sRUFBRSxDQUFRO0FBQ25CLENBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zdHlsZXMvYnJlYWtwb2ludC50cz85YmQ1Il0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjb25zdCBicmVha3BvaW50ID0ge1xyXG4gIG1vYmlsZTogXCIzNzVweFwiLFxyXG4gIHRhYmxldDogXCI2MDBweFwiLFxyXG4gIGxhcHRvcDogXCIxMjAwcHhcIixcclxuICBkZXNrdG9wOiBcIjE2MDBweFwiLFxyXG59O1xyXG4iXSwibmFtZXMiOlsiYnJlYWtwb2ludCIsIm1vYmlsZSIsInRhYmxldCIsImxhcHRvcCIsImRlc2t0b3AiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./styles/breakpoint.ts\n");

/***/ }),

/***/ "./styles/color.ts":
/*!*************************!*\
  !*** ./styles/color.ts ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"colors\": () => (/* binding */ colors)\n/* harmony export */ });\nconst colors = {\n    textDark: \"#232526\",\n    textDarkHover: \"#17191a\",\n    colorBlack: \"#17191a\",\n    blackGrey: \"#393b3c\",\n    blackGrey1: \"#4e4e4e\",\n    blackGrey2: \"#616161\",\n    bgWhite: \"#e4e6eb\",\n    bgRed: \"rgb(239,0,0)\",\n    bgGreen: \"#baff29\",\n    bgGreenHover: \"#a3dd24\",\n    dark: \"#232526\",\n    green: \"#a3dd24\",\n    white: \"#fefefe\",\n    whiteGray: \"#e6e6e6\"\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zdHlsZXMvY29sb3IudHMuanMiLCJtYXBwaW5ncyI6Ijs7OztBQUFPLEtBQUssQ0FBQ0EsTUFBTSxHQUFHLENBQUM7SUFDbkJDLFFBQVEsRUFBQyxDQUFTO0lBQ2xCQyxhQUFhLEVBQUMsQ0FBUztJQUN2QkMsVUFBVSxFQUFDLENBQVM7SUFDcEJDLFNBQVMsRUFBQyxDQUFTO0lBQ25CQyxVQUFVLEVBQUMsQ0FBUztJQUNwQkMsVUFBVSxFQUFDLENBQVM7SUFDcEJDLE9BQU8sRUFBQyxDQUFTO0lBQ2pCQyxLQUFLLEVBQUMsQ0FBYztJQUNwQkMsT0FBTyxFQUFDLENBQVM7SUFDakJDLFlBQVksRUFBQyxDQUFTO0lBQ3RCQyxJQUFJLEVBQUMsQ0FBUztJQUNkQyxLQUFLLEVBQUMsQ0FBUztJQUNmQyxLQUFLLEVBQUMsQ0FBUztJQUNmQyxTQUFTLEVBQUMsQ0FBUztBQUN2QixDQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vc3R5bGVzL2NvbG9yLnRzPzUxZjAiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNvbnN0IGNvbG9ycyA9IHtcclxuICAgIHRleHREYXJrOlwiIzIzMjUyNlwiLFxyXG4gICAgdGV4dERhcmtIb3ZlcjpcIiMxNzE5MWFcIixcclxuICAgIGNvbG9yQmxhY2s6XCIjMTcxOTFhXCIsXHJcbiAgICBibGFja0dyZXk6XCIjMzkzYjNjXCIsXHJcbiAgICBibGFja0dyZXkxOlwiIzRlNGU0ZVwiLFxyXG4gICAgYmxhY2tHcmV5MjpcIiM2MTYxNjFcIixcclxuICAgIGJnV2hpdGU6XCIjZTRlNmViXCIsXHJcbiAgICBiZ1JlZDpcInJnYigyMzksMCwwKVwiLFxyXG4gICAgYmdHcmVlbjpcIiNiYWZmMjlcIixcclxuICAgIGJnR3JlZW5Ib3ZlcjpcIiNhM2RkMjRcIixcclxuICAgIGRhcms6XCIjMjMyNTI2XCIsXHJcbiAgICBncmVlbjpcIiNhM2RkMjRcIixcclxuICAgIHdoaXRlOlwiI2ZlZmVmZVwiLFxyXG4gICAgd2hpdGVHcmF5OlwiI2U2ZTZlNlwiXHJcbn1cclxuXHJcbiJdLCJuYW1lcyI6WyJjb2xvcnMiLCJ0ZXh0RGFyayIsInRleHREYXJrSG92ZXIiLCJjb2xvckJsYWNrIiwiYmxhY2tHcmV5IiwiYmxhY2tHcmV5MSIsImJsYWNrR3JleTIiLCJiZ1doaXRlIiwiYmdSZWQiLCJiZ0dyZWVuIiwiYmdHcmVlbkhvdmVyIiwiZGFyayIsImdyZWVuIiwid2hpdGUiLCJ3aGl0ZUdyYXkiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./styles/color.ts\n");

/***/ }),

/***/ "./styles/font.ts":
/*!************************!*\
  !*** ./styles/font.ts ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"font\": () => (/* binding */ font)\n/* harmony export */ });\nconst font = {\n    size: {\n        extraSmall: '14px',\n        small: '16px',\n        medium: '18px',\n        large: '20px',\n        extraLarge: '24px'\n    }\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zdHlsZXMvZm9udC50cy5qcyIsIm1hcHBpbmdzIjoiOzs7O0FBQU8sS0FBSyxDQUFDQSxJQUFJLEdBQUcsQ0FBQztJQUNqQkMsSUFBSSxFQUFFLENBQUM7UUFDSEMsVUFBVSxFQUFFLENBQU07UUFDbEJDLEtBQUssRUFBRSxDQUFNO1FBQ2JDLE1BQU0sRUFBRSxDQUFNO1FBQ2RDLEtBQUssRUFBRSxDQUFNO1FBQ2JDLFVBQVUsRUFBRSxDQUFNO0lBQ3BCLENBQUM7QUFDUCxDQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vc3R5bGVzL2ZvbnQudHM/MTliZiJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY29uc3QgZm9udCA9IHtcclxuICAgIHNpemU6IHtcclxuICAgICAgICBleHRyYVNtYWxsOiAnMTRweCcsXHJcbiAgICAgICAgc21hbGw6ICcxNnB4JyxcclxuICAgICAgICBtZWRpdW06ICcxOHB4JyxcclxuICAgICAgICBsYXJnZTogJzIwcHgnLFxyXG4gICAgICAgIGV4dHJhTGFyZ2U6ICcyNHB4JyxcclxuICAgICAgfSxcclxufVxyXG4iXSwibmFtZXMiOlsiZm9udCIsInNpemUiLCJleHRyYVNtYWxsIiwic21hbGwiLCJtZWRpdW0iLCJsYXJnZSIsImV4dHJhTGFyZ2UiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./styles/font.ts\n");

/***/ }),

/***/ "./styles/global.ts":
/*!**************************!*\
  !*** ./styles/global.ts ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ \"styled-components\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _color__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./color */ \"./styles/color.ts\");\n\n\nconst GlobalStyle = styled_components__WEBPACK_IMPORTED_MODULE_0__.createGlobalStyle`\n    *{\n    box-sizing: border-box;\n    }\n\n    body {\n        margin: 0;\n        padding: 0;\n        font-family:'Open Sans', sans-serif;\n        /* background-image:url(\"/image/icons\\ _\\ graphics/Asset 1.png\"), url(\"/image/icons\\ _\\ graphics/Asset 1.png\") ,linear-gradient(${_color__WEBPACK_IMPORTED_MODULE_1__.colors.dark} 85%,${_color__WEBPACK_IMPORTED_MODULE_1__.colors.dark} 20% ) ; */\n        background-image:linear-gradient(${_color__WEBPACK_IMPORTED_MODULE_1__.colors.dark} 85%,${_color__WEBPACK_IMPORTED_MODULE_1__.colors.dark} 20% ) ;\n        background-repeat: repeat-y, repeat-y;\n        background-position: left,right;\n\n    }\n\n    *::-webkit-scrollbar {\n  width: 7px;\n}\n\n/* Track */\n*::-webkit-scrollbar-track {\n  background:  ${_color__WEBPACK_IMPORTED_MODULE_1__.colors.dark}; \n}\n \n/* Handle */\n*::-webkit-scrollbar-thumb {\n  background:  ${_color__WEBPACK_IMPORTED_MODULE_1__.colors.green}; \n}\n\n/* Handle on hover */\n*::-webkit-scrollbar-thumb:hover {\n  background:  ${_color__WEBPACK_IMPORTED_MODULE_1__.colors.green}; \n}\n\n/* Make clicks pass-through */\n#nprogress {\n    pointer-events: none;\n  }\n  \n  #nprogress .bar {\n    background: linear-gradient(180deg, ${_color__WEBPACK_IMPORTED_MODULE_1__.colors.green} 0%, ${_color__WEBPACK_IMPORTED_MODULE_1__.colors.green} 100%);\n    position: fixed;\n    z-index: 1300;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 5px;\n  }\n  \n  /* Fancy blur effect */\n  #nprogress .peg {\n    display: block;\n    position: absolute;\n    right: 0px;\n    width: 100px;\n    height: 100%;\n    box-shadow: 0 0 10px ${_color__WEBPACK_IMPORTED_MODULE_1__.colors.green}, 0 0 5px ${_color__WEBPACK_IMPORTED_MODULE_1__.colors.green};\n    opacity: 1;\n  \n    -webkit-transform: rotate(3deg) translate(0px, -4px);\n    -ms-transform: rotate(3deg) translate(0px, -4px);\n    transform: rotate(3deg) translate(0px, -4px);\n  }\n  \n  .nprogress-custom-parent {\n    overflow: hidden;\n    position: relative;\n  }\n  \n  .nprogress-custom-parent #nprogress .bar {\n    position: absolute;\n  }\n`;\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (GlobalStyle);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zdHlsZXMvZ2xvYmFsLnRzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBcUQ7QUFDckI7QUFFaEMsS0FBSyxDQUFDRSxXQUFXLEdBQUdGLGdFQUFpQixDQUFDOzs7Ozs7Ozs7d0lBU2tHLEVBQUVDLCtDQUFXLENBQUMsS0FBSyxFQUFFQSwrQ0FBVyxDQUFDO3lDQUNoSSxFQUFFQSwrQ0FBVyxDQUFDLEtBQUssRUFBRUEsK0NBQVcsQ0FBQzs7Ozs7Ozs7Ozs7O2VBWTNELEVBQUVBLCtDQUFXLENBQUM7Ozs7O2VBS2QsRUFBRUEsZ0RBQVksQ0FBQzs7Ozs7ZUFLZixFQUFFQSxnREFBWSxDQUFDOzs7Ozs7Ozs7d0NBU1UsRUFBRUEsZ0RBQVksQ0FBQyxLQUFLLEVBQUVBLGdEQUFZLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7eUJBZ0JsRCxFQUFFQSxnREFBWSxDQUFDLFVBQVUsRUFBRUEsZ0RBQVksQ0FBQzs7Ozs7Ozs7Ozs7Ozs7OztBQWdCakU7QUFDQSxpRUFBZUMsV0FBVyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3N0eWxlcy9nbG9iYWwudHM/MjA2MiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBjcmVhdGVHbG9iYWxTdHlsZSB9IGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xyXG5pbXBvcnQgeyBjb2xvcnMgfSBmcm9tIFwiLi9jb2xvclwiO1xyXG5cclxuY29uc3QgR2xvYmFsU3R5bGUgPSBjcmVhdGVHbG9iYWxTdHlsZWBcclxuICAgICp7XHJcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgfVxyXG5cclxuICAgIGJvZHkge1xyXG4gICAgICAgIG1hcmdpbjogMDtcclxuICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OidPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gICAgICAgIC8qIGJhY2tncm91bmQtaW1hZ2U6dXJsKFwiL2ltYWdlL2ljb25zXFwgX1xcIGdyYXBoaWNzL0Fzc2V0IDEucG5nXCIpLCB1cmwoXCIvaW1hZ2UvaWNvbnNcXCBfXFwgZ3JhcGhpY3MvQXNzZXQgMS5wbmdcIikgLGxpbmVhci1ncmFkaWVudCgke2NvbG9ycy5kYXJrfSA4NSUsJHtjb2xvcnMuZGFya30gMjAlICkgOyAqL1xyXG4gICAgICAgIGJhY2tncm91bmQtaW1hZ2U6bGluZWFyLWdyYWRpZW50KCR7Y29sb3JzLmRhcmt9IDg1JSwke2NvbG9ycy5kYXJrfSAyMCUgKSA7XHJcbiAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IHJlcGVhdC15LCByZXBlYXQteTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBsZWZ0LHJpZ2h0O1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAqOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDdweDtcclxufVxyXG5cclxuLyogVHJhY2sgKi9cclxuKjo6LXdlYmtpdC1zY3JvbGxiYXItdHJhY2sge1xyXG4gIGJhY2tncm91bmQ6ICAke2NvbG9ycy5kYXJrfTsgXHJcbn1cclxuIFxyXG4vKiBIYW5kbGUgKi9cclxuKjo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gIGJhY2tncm91bmQ6ICAke2NvbG9ycy5ncmVlbn07IFxyXG59XHJcblxyXG4vKiBIYW5kbGUgb24gaG92ZXIgKi9cclxuKjo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWI6aG92ZXIge1xyXG4gIGJhY2tncm91bmQ6ICAke2NvbG9ycy5ncmVlbn07IFxyXG59XHJcblxyXG4vKiBNYWtlIGNsaWNrcyBwYXNzLXRocm91Z2ggKi9cclxuI25wcm9ncmVzcyB7XHJcbiAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxuICB9XHJcbiAgXHJcbiAgI25wcm9ncmVzcyAuYmFyIHtcclxuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgxODBkZWcsICR7Y29sb3JzLmdyZWVufSAwJSwgJHtjb2xvcnMuZ3JlZW59IDEwMCUpO1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgei1pbmRleDogMTMwMDtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNXB4O1xyXG4gIH1cclxuICBcclxuICAvKiBGYW5jeSBibHVyIGVmZmVjdCAqL1xyXG4gICNucHJvZ3Jlc3MgLnBlZyB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHJpZ2h0OiAwcHg7XHJcbiAgICB3aWR0aDogMTAwcHg7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBib3gtc2hhZG93OiAwIDAgMTBweCAke2NvbG9ycy5ncmVlbn0sIDAgMCA1cHggJHtjb2xvcnMuZ3JlZW59O1xyXG4gICAgb3BhY2l0eTogMTtcclxuICBcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoM2RlZykgdHJhbnNsYXRlKDBweCwgLTRweCk7XHJcbiAgICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoM2RlZykgdHJhbnNsYXRlKDBweCwgLTRweCk7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgzZGVnKSB0cmFuc2xhdGUoMHB4LCAtNHB4KTtcclxuICB9XHJcbiAgXHJcbiAgLm5wcm9ncmVzcy1jdXN0b20tcGFyZW50IHtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgfVxyXG4gIFxyXG4gIC5ucHJvZ3Jlc3MtY3VzdG9tLXBhcmVudCAjbnByb2dyZXNzIC5iYXIge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIH1cclxuYDtcclxuZXhwb3J0IGRlZmF1bHQgR2xvYmFsU3R5bGUiXSwibmFtZXMiOlsiY3JlYXRlR2xvYmFsU3R5bGUiLCJjb2xvcnMiLCJHbG9iYWxTdHlsZSIsImRhcmsiLCJncmVlbiJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./styles/global.ts\n");

/***/ }),

/***/ "./styles/theme.ts":
/*!*************************!*\
  !*** ./styles/theme.ts ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"theme\": () => (/* binding */ theme)\n/* harmony export */ });\n/* harmony import */ var _color__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./color */ \"./styles/color.ts\");\n/* harmony import */ var _font__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./font */ \"./styles/font.ts\");\n/* harmony import */ var _breakpoint__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./breakpoint */ \"./styles/breakpoint.ts\");\n/* harmony import */ var _boxModel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./boxModel */ \"./styles/boxModel.ts\");\n\n\n\n\nconst theme = {\n    colors: _color__WEBPACK_IMPORTED_MODULE_0__.colors,\n    font: _font__WEBPACK_IMPORTED_MODULE_1__.font,\n    breakpoint: _breakpoint__WEBPACK_IMPORTED_MODULE_2__.breakpoint,\n    boxModel: _boxModel__WEBPACK_IMPORTED_MODULE_3__.boxModel\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zdHlsZXMvdGhlbWUudHMuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBZ0M7QUFDSDtBQUNZO0FBQ0o7QUFHOUIsS0FBSyxDQUFDSSxLQUFLLEdBQWdCLENBQUM7SUFDL0JKLE1BQU07SUFDTkMsSUFBSTtJQUNKQyxVQUFVO0lBQ1ZDLFFBQVE7QUFDWixDQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vc3R5bGVzL3RoZW1lLnRzPzY5NmIiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgY29sb3JzIH0gZnJvbSAnLi9jb2xvcic7XHJcbmltcG9ydCB7IGZvbnQgfSBmcm9tICcuL2ZvbnQnO1xyXG5pbXBvcnQgeyBicmVha3BvaW50IH0gZnJvbSAnLi9icmVha3BvaW50JztcclxuaW1wb3J0IHsgYm94TW9kZWwgfSBmcm9tICcuL2JveE1vZGVsJztcclxuaW1wb3J0IHsgRGVmYXVsdFRoZW1lIH0gZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xyXG5cclxuZXhwb3J0IGNvbnN0IHRoZW1lOkRlZmF1bHRUaGVtZSA9IHtcclxuICAgIGNvbG9ycyxcclxuICAgIGZvbnQsXHJcbiAgICBicmVha3BvaW50LFxyXG4gICAgYm94TW9kZWxcclxufVxyXG5cclxuIl0sIm5hbWVzIjpbImNvbG9ycyIsImZvbnQiLCJicmVha3BvaW50IiwiYm94TW9kZWwiLCJ0aGVtZSJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./styles/theme.ts\n");

/***/ }),

/***/ "./node_modules/react-toastify/dist/ReactToastify.css":
/*!************************************************************!*\
  !*** ./node_modules/react-toastify/dist/ReactToastify.css ***!
  \************************************************************/
/***/ (() => {



/***/ }),

/***/ "./node_modules/slick-carousel/slick/slick-theme.css":
/*!***********************************************************!*\
  !*** ./node_modules/slick-carousel/slick/slick-theme.css ***!
  \***********************************************************/
/***/ (() => {



/***/ }),

/***/ "./node_modules/slick-carousel/slick/slick.css":
/*!*****************************************************!*\
  !*** ./node_modules/slick-carousel/slick/slick.css ***!
  \*****************************************************/
/***/ (() => {



/***/ }),

/***/ "./styles/theme.scss":
/*!***************************!*\
  !*** ./styles/theme.scss ***!
  \***************************/
/***/ (() => {



/***/ }),

/***/ "@reduxjs/toolkit":
/*!***********************************!*\
  !*** external "@reduxjs/toolkit" ***!
  \***********************************/
/***/ ((module) => {

"use strict";
module.exports = require("@reduxjs/toolkit");

/***/ }),

/***/ "next-i18next":
/*!*******************************!*\
  !*** external "next-i18next" ***!
  \*******************************/
/***/ ((module) => {

"use strict";
module.exports = require("next-i18next");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ "nprogress":
/*!****************************!*\
  !*** external "nprogress" ***!
  \****************************/
/***/ ((module) => {

"use strict";
module.exports = require("nprogress");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/***/ ((module) => {

"use strict";
module.exports = require("react-redux");

/***/ }),

/***/ "react-toastify":
/*!*********************************!*\
  !*** external "react-toastify" ***!
  \*********************************/
/***/ ((module) => {

"use strict";
module.exports = require("react-toastify");

/***/ }),

/***/ "react-use":
/*!****************************!*\
  !*** external "react-use" ***!
  \****************************/
/***/ ((module) => {

"use strict";
module.exports = require("react-use");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "styled-components":
/*!************************************!*\
  !*** external "styled-components" ***!
  \************************************/
/***/ ((module) => {

"use strict";
module.exports = require("styled-components");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/***/ ((module) => {

"use strict";
module.exports = require("path");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/_app.tsx"));
module.exports = __webpack_exports__;

})();